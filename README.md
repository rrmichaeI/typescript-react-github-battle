<h1 align="center">
  <a href="https://ui.dev">
    <img
      src="https://ui.dev/images/logos/ui.png"
      alt="UI.dev Logo" width="300" />
  </a>
  <br />
</h1>

<h3 align="center">TypeScript with React Course Project - <a href="http://github-battle.ui.dev/">Github Battle App</a></h3>

### Info

This is the repository for UI.dev's "TypeScript with React" course project.

For more information on the course, visit **[ui.dev/react-typescript/](https://ui.dev/react-typescript/)**.

### Project

This project is a "Github Battle" app. You'll be able to see the most popular repos for a variety of languages as well as battle two Github users to see who has the better profile.

You can view the final project at **[github-battle.ui.dev/](http://github-battle.ui.dev/)**

### Project Preview

|                                                                                                                                                         Light Mode                                                                                                                                                         |                                                                                                                                                         Dark Mode                                                                                                                                                          |
| :------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------: | :------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------: |
| ![](https://user-images.githubusercontent.com/2933430/53439193-c39e1f00-39be-11e9-848f-d327f0ef5f53.png) ![](https://user-images.githubusercontent.com/2933430/53439196-c39e1f00-39be-11e9-875e-6f4aea52f099.png) ![](https://user-images.githubusercontent.com/2933430/53439197-c39e1f00-39be-11e9-8d17-d303692e5dd2.png) | ![](https://user-images.githubusercontent.com/2933430/53439194-c39e1f00-39be-11e9-8302-dcea6dae726a.png) ![](https://user-images.githubusercontent.com/2933430/53439195-c39e1f00-39be-11e9-9d10-488311266460.png) ![](https://user-images.githubusercontent.com/2933430/53439198-c39e1f00-39be-11e9-8bb8-d12687113a2e.png) |
